# BHAX

A BHAX csatorna forráskódjai. 
(Élő adások: https://www.twitch.tv/nbatfai, archívum: https://www.youtube.com/c/nbatfai)
A csatorna célja a szervezett számítógépes játék és a programozás népszerűsítése, tanítása, különös tekintettel a mesterséges intelligenciára! 
Hosszabb távon utánpótlás esport csapatok szervezésének tartalmi támogatása. 


A könyvem linkje: https://gitlab.com/TheZsolessz/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/bhax-textbook-fdl.pdf
