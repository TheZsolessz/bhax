#include <stdio.h>

int main(){
	int a = 5;
	int b = 9;

	printf("A: %d\n", a);
	printf("B: %d\n", b);

	printf("Csere segédváltozóval: \n");
	
	int temp = a;
	a = b;
	b = temp;
	
	printf("A: %d\n", a);
	printf("B: %d\n", b);
	
	printf("Csere segédváltozó nélkül: \n");

	a = a*b;
	b = a/b;
	a = a/b; 

	printf("A: %d\n", a);
	printf("B: %d\n", b);

	printf("Csere XOR módszerrel: \n");

	a = a^b;
	b = a^b;
	a = a^b; 

	printf("A: %d\n", a);
	printf("B: %d\n", b);
}
