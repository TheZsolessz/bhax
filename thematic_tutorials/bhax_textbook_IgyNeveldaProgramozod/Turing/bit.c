#include <stdio.h>

int main(){

    int x = 1; //változók felvétele
    int y = 0;
    
    while(x != 0)
    {
        x = x << 1; //bit shiftelés balra
        y++; // y értékét növeljük addig amíg x értéke nem lesz 0
    }
    
    printf("%d\n", y); //eredmény kiírása
    
    return 0;
}
