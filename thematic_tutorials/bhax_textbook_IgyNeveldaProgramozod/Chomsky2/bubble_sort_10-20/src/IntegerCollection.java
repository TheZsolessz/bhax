import java.util.Arrays;

public class IntegerCollection {

    int[] array;
    int size;
    int index= 0;
    boolean sorted = true;

    public IntegerCollection( int size) {
        this.size = size;
        this.array = new int[size];
    }

    public void add(int num){
        array[index++] = num;
    }

    public int[] sort(){
        for(int i=0; i<size-1; i++){
            for(int j=0; j<size-1; j++){
                if(array[j] > array[j+1]){
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        }
        sorted = true;
        return array;
    }

    public boolean contains(int num){
        if(!sorted)
            sort();

        int left = 0;
        int right = size-1;
        while(left <=right){
            int mid = left + (right - left) /2;

            if(array[mid] == num)
                return true;
            if(array[mid] < num) {
                left = mid + 1;
            } else{
                right = mid - 1;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
