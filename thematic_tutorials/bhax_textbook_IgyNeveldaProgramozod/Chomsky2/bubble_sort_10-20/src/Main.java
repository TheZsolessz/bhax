import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please give me your array's size: ");
        int size = sc.nextInt();
        IntegerCollection coll = new IntegerCollection(size);
        for(int i=0; i<size; i++){
            System.out.print("Give me your number: ");
            int num = sc.nextInt();
            coll.add(num);
        }
        System.out.print("Array: ");
        System.out.println(coll);
        coll.sort();
        System.out.print("Sorted array: ");
        System.out.println(coll);
        System.out.println("Contains 5? " + coll.contains(5));
        System.out.println("Contains 50? " + coll.contains(50));

    }
}
