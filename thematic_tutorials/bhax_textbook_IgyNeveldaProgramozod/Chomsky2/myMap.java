import java.util.*;

public class myMap {

    class OwnHashMap<K,V> implements Map<K,V> {

        private class Node<K,V> implements Entry<K,V>
        {
            public K key;
            public V value;
            public Node<K,V> next;

            public Node(K key,V value)
            {
                this.key = key;
                this.value = value;
                this.next = null;
            }

            public void add(K key,V value)
            {
                if(this.next == null)
                    this.next = new Node<K,V>(key,value);
                else
                    this.next.add(key,value);
            }
            public void remove(K key)
            {
                if(this.next != null && this.next.key.equals(key))
                {
                    System.out.println("111");
                    this.next = this.next.next;
                }
                else if(this.next != null)
                {System.out.println("2111");
                    this.next.remove(key);
                }

            }

            @Override
            public K getKey() {
                return key;
            }

            @Override
            public V getValue() {
                return value;
            }

            @Override
            public V setValue(V value) {
                this.value = value;

                return value;
            }
        }

        private Node [] collection;
        private int elems;

        public OwnHashMap(int size)
        {
            this.collection = new Node[size];
            this.elems = 0;
        }

        @Override
        public int size() {
            return elems;
        }

        @Override
        public boolean isEmpty() {
            return elems == 0;
        }

        @Override
        public boolean containsKey(Object key) {

            K k = (K)key;

            int pos = Math.abs(k.hashCode() % this.collection.length);

            if(collection[pos] != null)
            {
                if(collection[pos].key.equals(k)) return true;
                else
                {
                    Node<K,V> temp = collection[pos];
                    while(temp != null)
                    {
                        if(temp.key.equals(k)) return true;
                        temp = temp.next;
                    }
                }
            }

            return false;
        }

        @Override
        public boolean containsValue(Object value) {

            V v = (V)value;

            for(Node<K,V> elem : this.collection)
            {
                if(elem != null)
                {
                    if(elem.value.equals(value)) return true;
                    else
                    {
                        Node<K,V> temp = elem;
                        while(temp != null)
                        {
                            if(temp.value.equals(value)) return true;
                            temp = temp.next;
                        }
                    }
                }
            }

            return false;
        }

        @Override
        public V get(Object key) {

            K k = (K)key;

            int pos = Math.abs(k.hashCode() % this.collection.length);

            if(collection[pos] != null)
            {
                if(collection[pos].key.equals((k))) return (V)collection[pos].value;
                else
                {
                    Node<K,V> temp = collection[pos];
                    while(temp != null)
                    {
                        if(temp.key.equals(k)) return temp.value;
                        temp = temp.next;
                    }
                }
            }

            return null;
        }

        @Override
        public V put(K key, V value) {
            try {
                int pos = Math.abs(key.hashCode() % this.collection.length);

                if(!this.containsKey(key))
                {
                    if(collection[pos] == null)
                        collection[pos] = new Node<K,V>(key,value);
                    else
                    {
                        collection[pos].add(key,value);
                    }
                    elems++;
                }
                else
                {
                    // contains key
                    if(collection[pos].key.equals(key))
                        collection[pos].value = value;
                    else
                    {
                        Node<K,V> temp = collection[pos];
                        while(temp != null)
                        {
                            if(temp.key.equals(key))
                            {
                                temp.value = value;
                                break;
                            }
                            temp = temp.next;
                        }
                    }
                }

                return value;
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
            return null;
        }

        @Override
        public V remove(Object key) {

            K k = (K)key;

            if(!containsKey(k)) return null;

            int pos = Math.abs(k.hashCode() % this.collection.length);

            V val = null;

            if(collection[pos].key.equals(k))
            {

                val = (V)collection[pos].value;
                if(collection[pos].next != null)
                {
                    collection[pos] = collection[pos].next;
                }
                else
                {
                    collection[pos] = null;
                }

            }
            else
            {
                collection[pos].remove(k);
            }

            this.elems--;

            return val;
        }

        @Override
        public void putAll(Map<? extends K, ? extends V> m) {

            if(m.size() + this.elems >= this.collection.length) return;

            // m.forEach(this::put);

            Object [] keys = m.keySet().toArray();
            Object [] values = m.values().toArray();

            for(int i=0;i<m.size();i++)
                put((K)keys[i],(V)values[i]);
        }

        @Override
        public void clear() {
            this.collection = new Node[this.collection.length];
            this.elems = 0;
        }

        @Override
        public Set<K> keySet()
        {
            Set<K> keys = new HashSet<K>();

            for(int i=0;i<this.collection.length;i++)
            {
                Node<K,V> temp = collection[i];
                while(temp != null)
                {
                    keys.add(temp.key);
                    temp = temp.next;
                }
            }

            return keys;
        }

        @Override
        public Collection<V> values() {
            ArrayList<V> vls = new ArrayList<V>();

            for(int i=0;i<this.collection.length;i++)
            {
                Node<K,V> temp = collection[i];
                while(temp != null)
                {
                    vls.add(temp.value);
                    temp = temp.next;
                }
            }

            return vls;
        }

        @Override
        public Set<Entry<K, V>> entrySet() {
            Set<Entry<K,V>> entrySet = new HashSet<Entry<K,V>>();

            for(int i=0;i<this.collection.length;i++)
            {
                Node<K,V> temp = collection[i];
                while(temp != null)
                {
                    entrySet.add(temp);
                    temp = temp.next;
                }
            }

            return entrySet;
        }
    }


}
