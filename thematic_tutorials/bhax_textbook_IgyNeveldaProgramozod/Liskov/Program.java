public class Program {
    static class Vehicle {
        public Vehicle() {
            System.out.println("Vehicle constructor");
        }
        public void start() {
            System.out.println("Vehicle started.");
            //alap start metódus
        }
    }
    static class Car extends Vehicle {
        public Car() {
            System.out.println("Car constructor");
        }
        @Override
        public void start() {
            System.out.println("Car started.");
            // Felülírjuk az alap start metódust
        }
    }
    static class Supercar extends Car {
        public Supercar() {
            System.out.println("Supercar constructor");
        }
        @Override
        public void start() {
            System.out.println("Supercar started.");
            // Felülírjuk az alap start metódust
        }
    }

    public static void main(String[] args) {
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);
        System.out.println("-----");

        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);
        System.out.println("-----");

        //Supercar thirdVehicle = new Vehicle();
        //thirdVehicle.start();


    }

}
