#!/usr/bin/clisp

(defun fact_it (n)
	(let ((f 1))
		(dotimes (i n)
		(setf f (* f (+ i 1))))
		f
		)
)

(defun fact (n)
	(if (= n 0)
		1
		(* n (fact(- n 1)))))

(format t "Recursive: ~%")

(loop for i from 0 to 20
	do (format t "~D! = ~D~%" i (fact i)))

(format t "Iterative: ~%")

(loop for i from 0 to 20
	do (format t "~D! = ~D~%" i (fact_it i)))