<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Stroustrup!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>EPAM: It's gone. Or is it?</title>
        <para> Adott a következő osztály: 
        <programlisting language="java">
        <![CDATA[
public class BugousStuffProducer {
    private final Writer writer;
    public BugousStuffProducer(String outputFileName) throws
IOException {
        writer = new FileWriter(outputFileName);
    }
    public void writeStuff() throws IOException {
        writer.write("Stuff");
    }
    @Override
    public void finalize() throws IOException {
        writer.close();
    }
}
        ]]>
        </programlisting>
        Mutass példát arra az esetre, amikor előfordulhat, hogy bár a program futása során meghívtuk a <programlisting language="java"><![CDATA[writeStuff()]]></programlisting> metódust, a fájl, amibe írtunk még is üres.
Magyarázd meg, miért. Mutass alternatívát.
        </para>
        <para>
<programlisting language="java"><![CDATA[
public static void main(String[] args) throws IOException{
    BugousStuffProducer w = new BugousStuffProducer("valami.txt");
    w.writeStuff();
}]]></programlisting>
Ha ezt a kódot használjuk, akkor semmi nem fog megjelenni a fájlunkban, mert a finalize metódus nem került meghívásra, így a fájlunk nincs lezárva. Ez a lezárásos probléma vagy a finalize meghívásával, vagy a writer.flush(); kód writeStuff() metódusba való beillesztésével küszöbölhető ki. Ezek a következőként nézhetnek ki:
<programlisting language="java"><![CDATA[
public static void main(String[] args) throws IOException{
    BugousStuffProducer w = new BugousStuffProducer("valami.txt");
    w.writeStuff();
    w.finalize();
}]]></programlisting>
<programlisting language="java">
        <![CDATA[
public class BugousStuffProducer {
    private final Writer writer;
    public BugousStuffProducer(String outputFileName) throws
IOException {
        writer = new FileWriter(outputFileName);
    }
    public void writeStuff() throws IOException {
        writer.write("Stuff");
        writer.flush();
    }
    @Override
    public void finalize() throws IOException {
        writer.close();
    }
}
        ]]>
        </programlisting>
</para>
    </section>

    <section>
        <title>EPAM: Kind of equal</title>
        <para> Adott az alábbi kódrészlet.
        <programlisting language="java">
        <![CDATA[// Given
String first = "...";
String second = "...";
String third = "...";
// When
var firstMatchesSecondWithEquals = first.equals(second);
var firstMatchesSecondWithEqualToOperator = first == second;
var firstMatchesThirdWithEquals = first.equals(third);
var firstMatchesThirdWithEqualToOperator = first == third;]]>
        </programlisting>
        Változtasd meg a String third = "..."; sort úgy, hogy a
firstMatchesSecondWithEquals, firstMatchesSecondWithEqualToOperator,
firstMatchesThirdWithEquals értéke true, a
firstMatchesThirdWithEqualToOperator értéke pedig false legyen. Magyarázd
meg, mi történik a háttérben.
        </para>
        <para>Java-ban az egyenlőségjellel történő összehasonlítás esetén azt nézzük meg, hogy ugyanarra a memóriacímre mutat-e a két objektum, tehát ha egy újat hozunk létre, akkor false értéket kapunk a megfelelő helyen. Ez a következőképpen történhet: 
<programlisting language="java">
<![CDATA[String third = new String("...");]]>
        </programlisting></para>
    </section>

    <section>
        <title>EPAM: Java GC</title>
        <para>
Mutasd be nagy vonalakban hogyan működik Java-ban a GC (Garbage Collector).
</para>
<para>Források:</para>
<para><link xlink:href="https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82">
<filename>https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82</filename></link>
<link xlink:href="https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror">
<filename>https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror</filename></link>
</para>
        <para>
         Java-ban automatikus GC azaz Garbage Collector működik. A Java egy olyan OO nyelv, amely automatikusan foglal és szabadít fel helyet a memóriában az objektumainak, így ezzel a programozónak nem kell foglalkoznia. A Java 12 hét fajta GC-t tartalmaz, ezek a következők:</para>
<para>Serial Garbage Collector: Ez a legegyszerűbb GC implementáció, egy szálon futó alkalmazásokhoz tervezték, mindne szálat fagyaszt amikor fut.</para>
<para>Parallel Garbage Collector: Hasonlóan működik, mint az előző, csak ez több szálon fut. Olyan alkalmazásoknál használják, ahol nem probléma, hogy egy pillanatra megáll a működés.</para>
<para>CMS Garbage Collector: Olyan alkalmazásoknál használják, ahol fontos, hogy a program megállása csak nagyon rövid időre következzen be.</para>
<para>G1 Garbage Collector: Multiprocesszoros és nagy memóriával rendelkező gépeknél használják. A Heap memóriát részekre bontja és a parallelt futtatja rajta.</para>
<para>Epsilon Garbage Collector: Ez egy passzív GC, amely lehetővé teszi a memóriafoglalást, de a felszabadítást nem végzi el, így ezesetben megjelenhet az "Out of memory" hiba.</para>
<para>Z Garbage Collector: Ezen GC előnye az, hogy a megállítást max 10ms-ig engedélyezi, ezzel szemben a G1 és a Parallel Collectorok nagyjából 200ms megállást eredményeznek. Ez észrevehető különbséghez vezet.</para>
<para>Shenandoah: A Shenandoah egy nagyon alacsony megállítási idővel rendelkező GC, egyszerre több GC feladatot használ a programon.</para>
    </section>

    <section>
        <title>Másoló-mozgató szemantika</title>
        <para>Kódcsipeteken keresztül vesd össze a C++11 másoló és mozgató szemantikáját, a mozgató konstruktort alapozd a mozgató értékadásra!</para>
        <para>
<programlisting language="C++">
<![CDATA[LZWBinFa (const LZWBinFa & eredeti){  //másoló konstruktor esetén az eredeti referenciáját használjunk. const típust alkalmazunk
        
std::cout << "másoló konstruktor\n";
gyoker = masol(eredeti.gyoker, eredeti.fa);  //a másol függvényünkkel átmásoljuk az eredeti gyökeret és fát a jelenlegi példányunkba.
}]]>
</programlisting></para>
        <para>
<programlisting language="C++">
<![CDATA[LZWBinFa (LZWBinFa && eredeti){
std::cout << "mozgató konstruktor\n";

gyoker = nullptr;  //mozgató konstruktornál null értéket adunk a gyökérnek
*this = std::move(eredeti);  //majd at std::move() függvénnyel áthúzzuk az eredetit a jelenlegi példányba.
}]]>
</programlisting></para>
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </chapter>                
