package student;

import subject.Subject;

import java.util.Date;
import java.util.Set;

public class Student {

    private String name;
    private Date born;
    private Set<Subject> subjects;

    public String getName(){
        return name;
    }

    public Set<Subject> getSubjects(){
        return subjects;
    }

    public void addSubject(Subject subject){
        this.subjects.add(subject);
    }

    public void removeSubject(Subject subject){
        this.subjects.remove(subject);
    }

    public void getData(){
        System.out.println(name+" született: "+born+" .");
    }

}
