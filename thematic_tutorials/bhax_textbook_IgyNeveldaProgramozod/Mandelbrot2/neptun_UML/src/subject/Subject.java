package subject;

import lecturer.Lecturer;
import student.Student;

import java.util.Set;

public class Subject {

    private String name;
    private String code;
    private String room;
    private String time;
    private Set<Lecturer> lecturers;
    private Set<Student> students;

    public String getName(){
        return name;
    }

    public String getCode(){
        return code;
    }

    public String getRoom(){
        return room;
    }

    public String getTime(){
        return time;
    }

    public Set<Student> getStudents(){
        return students;
    }

    public Set<Lecturer> getLecturers(){
        return lecturers;
    }

    public void addStudent(Student student){
        this.students.add(student);
    }

    public void addLecturer(Lecturer lecturer){
        this.lecturers.add(lecturer);
    }

}
