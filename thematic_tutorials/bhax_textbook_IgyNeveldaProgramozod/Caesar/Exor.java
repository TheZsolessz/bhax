public class Exor {
    /* A program használatakor az alábbi argumentumokat vesszük be inputként (a konstruktornak): 

    Egy kulcsot
    Egy input streamet
    Egy output streamet

    */
    public Exor(String kulcsSzöveg, 
            java.io.InputStream bejövőCsatorna,
            java.io.OutputStream kimenőCsatorna)
            throws java.io.IOException {

        byte [] kulcs = kulcsSzöveg.getBytes(); //a kulcsot berakjuk egy byte típusú tömbbe
        byte [] buffer = new byte[256]; //létrehozunk egy 256 méretű buffert

        int kulcsIndex = 0;
        int olvasottBájtok = 0; //olvasott bájtok száma

        while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) { //beolvassuk a szöveget
            
            for(int i=0; i<olvasottBájtok; ++i) {   //for ciklus 

                // Majd itt titkosítjuk a kulccsal a szövegünket (xorral)

                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex+1) % kulcs.length;
                
            }
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);   
        }
    }

    public static void main(String[] args) {
        
        try {
            
            new Exor(args[0], System.in, System.out); // Létrehozunk egy új ExorTitkosító objektumot, melynek átadjuk a megfelelő paramétereket (kulcs az első argumentum,inputStream,outputStream)
            
        } catch(java.io.IOException e) {

            // Hibakezelés
            
            e.printStackTrace();
            
        }   
    }
}